import { useState } from "react"
import Card, {params as paramsCard} from "@react/card"
import Grid from '@react/grid'
import CardProduct from '@react/product'

const App = (): JSX.Element => {

	const [data, setData] = useState<paramsCard[]>([
		{value: '1', image: '/vite.svg', title: 'Tienda 1'},
		{value: '2', image: '/vite.svg', title: 'Tienda 2'},
		{value: '3', image: '/vite.svg', title: 'Tienda 3'},
		{value: '4', image: '/vite.svg', title: 'Tienda 4'},
		{value: '5', image: '/vite.svg', title: 'Tienda 5'},
		{value: '6', image: '/vite.svg', title: 'Tienda 6'},
		{value: '6', image: '/vite.svg', title: 'Tienda 6'},
		{value: '7', image: '/vite.svg', title: 'Tienda 7'},
	])
	
	const handleSelect: paramsCard["onClick"] = (value) => { 
		console.log(value)
	}
 
  	return (
		<Grid>
			<>
			<CardProduct value="1" image="/product.jpg" title="Telefono" 
				onMouseLeave={()=>console.log('leave')}
				onMouseOver={()=>console.log('over')}
			/>
			<CardProduct value="2" image="/product.jpg" title="Telefono" />
			{
				data.map((v, i) => <Card key={i} {...v} onClick={handleSelect}/>)
			}
			</>
		</Grid>
	)

}

export default App

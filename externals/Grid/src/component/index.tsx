import React from "react";
import styled from "styled-components";

const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: flex-start;
    width: 100%;
    height: 100%;
    overflow: scroll;
    gap: 15px;
`

export interface params {
  children: React.ReactNode;
  style?: React.CSSProperties;
}

const App = (params: params): JSX.Element => {

    return (
        <Container style={params.style}>
            {params.children}
        </Container>
    );

};


export default App;
import React from "react"
import styled from "styled-components"

const Container = styled.div`
	display: flex;
	flex-direction: column;
	width: 150px;
	
`
const Img = styled.img`
	aspect-ratio: 9/16;
	object-fit: cover;
`

const Body = styled.div`
	display: flex;
	flex-direction: column;
	padding-left: 10px;
	padding-right: 10px;
	background-color: #1c1719;
	border-radius: 5px;
`

const Title = styled.p`
	color: #fff;
`

const Description = styled.p`
`

export interface params {
	value: string;
	image: string;
	title: string;
	description?: string;
	onClick?: ()=>void;
	onMouseLeave?: React.MouseEventHandler<HTMLDivElement>;
	onMouseOver?: React.MouseEventHandler<HTMLDivElement>;
}

const defaultProps = {

}

/**
 * 
 * @param params
 * @param params.value identificador del producto
 * @param params.image Imagen del producto
 * @param params.title Titulo del producto
 * @param params.description Descripcion del producto - opcional
 * @param params.onClick Callback - opcional
 * @param params.onMouseLeave Callback - opcional
 * @param params.onMouseOver Callback - opcional
 * @example 
 * 	import { useState } from "react"
	import CardProduct from '@react/product'
	const App = (): JSX.Element => {

		return (
			<CardProduct value="1" image="/product.jpg" title="Telefono" 
				onMouseLeave={()=>console.log('leave')}
				onMouseOver={()=>console.log('over')}
			/>
		)

	}
	export default App
  
 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {
	params = { ...defaultProps, ...params }

	const handleClick = () => { 

		if(typeof params.onClick === 'function') params.onClick()
	}
	return (
		<Container onClick={handleClick} onMouseLeave={params.onMouseLeave} onMouseOver={params.onMouseOver}>
			<Img src={params.image} />
			<Body>
				<Title>{params.title}</Title>
				<Description>{params.description}</Description>
			</Body>
		</Container>
	)
}

export default App
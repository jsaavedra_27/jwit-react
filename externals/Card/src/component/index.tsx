import React from 'react'
import styled from "styled-components"
import Avatar from '@react/avatar'

const Container = styled.div`
	background-color: red;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding: 30px 15px;
	width: 123px;
	height: 160px;
	border-radius: 6px;
`

const Title = styled.p``
const Description = styled.p``

export interface params {
	value: string;
	title: string;
	description?: string;
	image: string;
	onClick?: (value: string) => void;
}

/**
 * 
 * @param params 
 * @param params.value identificador del elemento
 * @param params.title title del elemento
 * @param params.description description del elemento
 * @param params.image image del elemento
 * @param params.onClick callback que retorna el identificador al presionar sobre el contenedor
 * @returns JSX.Element
 * @example
 * import { useState } from "react"
import Card, {params as paramsCard} from "./components/Card"
import Grid from "./components/Grid"

const App = (): JSX.Element => {

	const [data, setData] = useState<paramsCard[]>([
		{value: '1', image: '/vite.svg', title: 'Tienda 1'},
		{value: '2', image: '/vite.svg', title: 'Tienda 2'},
		{value: '3', image: '/vite.svg', title: 'Tienda 3'},
	])
	
  	return (
		<Grid>
			{
				data.map((v, i) => <Card key={i} {...v}/>)
			}
		</Grid>
	)

}

export default App
 */
const App = (params: params): JSX.Element => {

	const handleClick = () => { 
		if(typeof params.onClick === 'function') params.onClick(params.value)
	}
  	return (
		<Container onClick={handleClick}>
			<Avatar image={params.image}/>
			<Title>{params.title}</Title>
			<Description>{params.description}</Description>
		</Container>
	)

}

export default App
